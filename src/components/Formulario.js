import React, {useState} from 'react';
import styled from '@emotion/styled';
import {obtenerDiferenciaYear, calcularIncrementoPorMarca, calcularIncrementoPorPlan} 
from '../helper.js';
import PropTypes from 'prop-types'; //Para documentar el codigo

//Styled Component para aplicar estilo css a los div
const Campo = styled.div`
    display: flex;
    margin-bottom: 1rem;
    align-items: center; 
`;

//Styled Component para aplicar estilo css a los label
const Label = styled.label`
    flex: 0 0 100px;
`;

//Styled Component para aplicar estilo css a los select
//-webkit-appearance elimina apariencia nativa de los select
const Select = styled.select`
    display: block;
    width: 100%;
    padding: 1rem;
    border: 1px solid #e1e1e1;
    -webkit-appearance: none; 
`;

//Styled Component para aplicar estilo css a los radiobutton
const InputRadio = styled.input`
    margin: 0 1rem;
`;

//Styled Component para aplicar estilo css al boton
const Boton = styled.button`
    background-color: #00838F;
    font-size: 16px;
    width: 100%;
    padding: 1rem;
    color: #fff;
    text-transform: uppercase;
    font-weight: bold;
    border: none;
    transition: background-color .3s ease;
    margin-top: 2rem;

    &:hover { /*& hace referencia al mismo elemento. Parecido a SASS*/
        background-color: #26C6DA;
        cursor: pointer;
    }
`;

//Styled Component para aplicar estilo css a otro div
const Error = styled.div`
    background-color: red;
    color: white;
    padding: 1rem;
    width: 94%;
    text-align: center;
    margin-bottom: 2rem;
`;

//Componente Formulario, recibe funcion setResumen desde app
const Formulario = ({setResumen, verSpinner}) => {

    //State para recibir datos del formulario. Pude haber creado un State por campo tambien.
    //plan: 'completo' Si lo dejamos asi, se selecciona por defecto el segundo input radio
    const [datos, guardarDatos] = useState({
        marca: '',
        year: '',
        plan: ''
   });

    //Extraigo los valores (Destructuring)
    const {marca, year, plan} = datos;

    //Obtengo datos del formulario
    const obtenerDatosForm = e => {
        guardarDatos({
            ...datos,
            [e.target.name] : e.target.value
        });
    }

    //Para saber si existe algun error con los datos ingresados
    const [error, setError] = useState(false);

    //Validamos datos del formulario
    const cotizarSeguro = e => {
        e.preventDefault();

        if(marca.trim() === '' || year.trim() === '' || plan.trim() === '' ) {
            setError(true);
            return;
        }

        //Restauramos a false para que no se visualize mensaje de error
        setError(false);

        //Iniciamos con una base de 2000
        let resultado = 2000;

        //Obtenemos la diferencia de años
        const diferenciaYear = obtenerDiferenciaYear(year);
        //console.log(diferenciaYear);
        
        //Por cada año hay que restar el 3%
        resultado -= ((diferenciaYear * 3) * resultado) / 100;

        // Incremento según la Marca. Americano 15%, Asiatico 3%, Europeo 30%
        resultado = calcularIncrementoPorMarca(marca) * resultado;

        // Incremento según el Plan Basico 20%, Completo 50%
        //Aplicamos parseFloat y toFixed para manejar mejor los decimales.
        const incrementoPlan = calcularIncrementoPorPlan(plan);
        resultado = parseFloat( incrementoPlan * resultado).toFixed(2);
       
        //Mostrar Spinner
        verSpinner(true);
        
        //Un timeOut de 3seg para enviar el resumen. Esto es para lograr efecto visual del Spinner
        setTimeout(() => {
            
            //Ocultar Spinner
            verSpinner(false);

            //Total y Resumen de operacion
            setResumen({
                cotizacion: Number(resultado), //eliminamos warning de casteo de dato
                datos
            });
        }, 3000);
        
    };

    //Renderizado
    return (
        
        //Reemplazamos los <div>, <label> y <select> por su styled component Campo.  
        <form onSubmit = {cotizarSeguro}>
            
            {   //Aplicamos css al mensaje de error a traves de un Styled Component
                error 
                ? <Error>Todos los campos son obligatorios</Error>
                : null
            }

            <Campo>
                <Label>Marca</Label>
                <Select name = "marca" value = {marca} onChange = {obtenerDatosForm}>
                    <option value="">-- Seleccione --</option>
                    <option value="americano">Americano</option>
                    <option value="europeo">Europeo</option>
                    <option value="asiatico">Asiatico</option>
                </Select>
            </Campo>

            <Campo>
                <Label>Año</Label>
                <Select name = "year" value = {year} onChange = {obtenerDatosForm}>
                    <option value="">-- Seleccione --</option>
                    <option value="2021">2021</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                    <option value="2014">2014</option>
                    <option value="2013">2013</option>
                    <option value="2012">2012</option>
                </Select>
            </Campo>

            <Campo>
                <Label>Plan</Label>
                <InputRadio
                    type = "radio"
                    name = "plan"
                    value = "basico"
                    checked = {plan === "basico"} //Asi asociamos el input Radio
                    onChange = {obtenerDatosForm}
                /> Básico
                <InputRadio
                    type = "radio"
                    name = "plan"
                    value = "completo"
                    checked = {plan === "completo"} //Asi asociamos el input Radio
                    onChange = {obtenerDatosForm}
                /> Completo
            </Campo>
            <Boton type = "submit">Cotizar</Boton>
        </form>
    );
}

//Documentacion del componente
Formulario.propTypes = {
    setResumen : PropTypes.func.isRequired,
    verSpinner : PropTypes.func.isRequired 
}

export default Formulario;