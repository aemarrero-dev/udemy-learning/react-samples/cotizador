import React from 'react';
import styled from '@emotion/styled'; //Uso de Style Components
import PropTypes from 'prop-types'; //Para documentar el codigo

//Styled Component para aplicar estilo css al header
const ContenedorHeader = styled.header`
    background-color: #26C6DA;
    padding: 10px;
    font-weight: bold;
    color: #FFFFFF;
`;

//Styled Component para aplicar estilo css al h1
const TextoHeader = styled.h1`
    font-size: 2rem;
    margin: 0;
    font-family: 'Slabo 27px', serif;
    text-align: center;
`;

const Header = ({titulo}) => {
    return (
        //Reemplazamos <header> y <h1> por el Styled Component 
        <ContenedorHeader> 
            <TextoHeader>{titulo}</TextoHeader>
        </ContenedorHeader>
     );
}

//Documentacion del componente
Header.propTypes = {
    titulo: PropTypes.string.isRequired
}

export default Header;

/* 
Usamos libreria Emotion que permite mezclar codigo html con css y se aplican como componentes.
npm i @emotion/styled @emotion/core
Instalamos los siguientes plugins en VS
1. Style-Snippets - Fernando de la Madrid
2. vscode-styled-components - Julien Poissonnier
3. Styled-Components Snippets - Jon Wheeler
*/
