import React from 'react';
import styled from '@emotion/styled';
import {primeraLetraMayuscula} from '../helper';
import PropTypes from 'prop-types'; //Para documentar el codigo

//Styled Component para aplicar estilo css al div
const ContenedorResumen = styled.div`
    padding: 1rem;
    text-align: center;
    background-color: #00838F;
    color: #FFF;
    margin-top: 1rem;
`;

const Resumen = ({datos}) => {
    
    //Extraigo los valores de datos (Destructuring)
    const {marca, year, plan} = datos;

    if(marca === '' || year === '' || plan === '') return null;

    return ( //Reemplazamos el <Fragment> por el styled componente para div
        <ContenedorResumen>
            <h2>Resumen de Cotización</h2>
            <ul>
                <li>Marca: {primeraLetraMayuscula(marca)}</li>
                <li>Plan: {primeraLetraMayuscula(plan)}</li>
                <li>Year: {year}</li>
                
            </ul>
        </ContenedorResumen>
    );
}

//Documentacion del componente
Resumen.propTypes = {
    datos: PropTypes.object.isRequired
}

export default Resumen;