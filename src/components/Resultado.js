import React from 'react';
import styled from '@emotion/styled';
import PropTypes from 'prop-types'; //Para documentar el codigo

//Instalamos libreria de animacion. npm i react-transition-group
import {TransitionGroup, CSSTransition} from 'react-transition-group';

//Styled component para aplicar css al parrafo
const Mensaje = styled.p`
    background-color: rgb(127, 224, 237);
    margin-top: 2rem;
    padding: 1rem;
    text-align: center;
`;

//Styled component para aplicar css al div de Resultado
const ResultadoCotizacion = styled.div`
    text-align: center;
    padding: .5rem;
    border: 1px solid #26C6DA;
    background-color: rgb(127, 224, 237);
    margin-top: 1rem;
    position: relative;
`;

//Styled component para aplicar css al parrafo Total
const Total = styled.p`
    color: #00838F;
    padding: 1rem;
    text-transform: uppercase;
    font-weight: bold;
    margin: 0;
`;

const Resultado = ({cotizacion}) => {
    
    return ( 
        //Validamos cotizacion para visualizar o no el componente
        (cotizacion === 0) 
        ? <Mensaje>Elige marca, año y plan de seguro</Mensaje> 
        : 
            (
                <ResultadoCotizacion>
                    
                    <TransitionGroup 
                        component = "span" //Reemplazamos p por span. No puede haber un <p> dentro de otro.
                        className = "resultado">
                        <CSSTransition 
                            classNames= "resultado"
                            key = {cotizacion}
                            timeout = {{enter: 500, exit: 500}}
                        >
                            <Total> El total es: $ <span>{cotizacion}</span> </Total>
                        </CSSTransition>
                    </TransitionGroup>

                </ResultadoCotizacion>
            )
    );
}

//Documentacion del componente
Resultado.propTypes = {
    cotizacion: PropTypes.number.isRequired
}

export default Resultado;

/*
Instalamos libreria de animacion. npm i react-transition-group
TransitionGroup tiene como propiedades el tipo de componente a animar. En este caso <Total> es un <p>.
Y un nombre para la propiedad className que referencia a una clase css en index.css.
*/