//Funcion que retorna la diferencia entre el año actual y el año ingresado.
export function obtenerDiferenciaYear(year) {
    return new Date().getFullYear() - year;
}

//Calcula el total a pagar segun la marca
export function calcularIncrementoPorMarca(marca) {
    let incremento;

    switch(marca) {
        case 'europeo': 
            incremento = 1.30;
            break;
        case 'americano':
            incremento = 1.15;
            break;
        case 'asiatico':
            incremento = 1.05;
            break;
        default:
            break;
    }
    return incremento;
}

//Calcula el incremento por tipo de Plan
export function calcularIncrementoPorPlan(plan) {
    return (plan === 'basico') ? 1.20 : 1.50;
}

//Cambia texto ingresado por Texto. (Aplica mayuscula a la 1era letra)
export function primeraLetraMayuscula(texto){
    return texto.charAt(0).toUpperCase() + texto.slice(1);
}

