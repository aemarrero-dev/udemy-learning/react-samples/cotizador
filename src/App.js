import React, { useState } from 'react';
import Header from './components/Header';
import styled from '@emotion/styled'; //Uso de Style Components
import Formulario from './components/Formulario';
import Resumen from './components/Resumen';
import Resultado from './components/Resultado';
import Spinner from './components/Spinner';

//Styled Component para aplicar estilo al div
const Contenedor = styled.div`
  max-width: 600px;
  margin: 0 auto;
`;

const ContenedorFormulario = styled.div`
  background-color: #FFF;
  padding: 3rem;
`;

function App() {

  //State que obtendra el resultado de Formulario para luego ser enviado a otro componente.
  const [resumen, setResumen] = useState({
    cotizacion: 0,
    datos: {
      marca: '',
      year: '',
      plan: ''
    }
  });

  //State para gestionar la visualizacion del Spinner
  const [cargando, verSpinner] = useState(false);

  //Extraemos datos
  const {cotizacion, datos} = resumen;
  
  return (
    //Aplicamos estilo a todo el componente Header.
    <Contenedor>
      <Header
        titulo = 'Cotizador de Seguros'
      />

      <ContenedorFormulario>
        <Formulario
          setResumen = {setResumen}
          verSpinner = {verSpinner}
          
        />

        {cargando ? <Spinner /> : null}
        
        <Resumen
          datos = {datos}
        />

        { 
          !cargando
          ?  <Resultado cotizacion = {cotizacion}/>
          :  null
        }
       
      </ContenedorFormulario>
    </Contenedor>
   
  );
}

export default App;

